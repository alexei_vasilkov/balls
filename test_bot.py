import pygame as pg
import numpy as np
from numpy import linalg as la
import itertools
from math import atan2, pi, sin, cos, ceil, sqrt
import time
from quad_tree import QuadTree
#from profilehooks import profile
count = itertools.count()
def ord_pair(q1, q2):
    if q1.id > q2.id:
        return q2, q1
    else:
        return q1, q2

def norm(arr):
    return sqrt(arr[0]*arr[0] + arr[1]*arr[1])


def compute_circle_walls(c1, c2):
    dr = c2.pos - c1.pos
    d = norm(dr)
    if d < abs(c1.rad - c2.rad) or (d == 0 and c1.rad == c2.rad) or d > c1.rad + c2.rad:
        #print('circle center inside another circle')
        #print('CONDITIONS', d < abs(c1.rad - c2.rad), (d == 0 and c1.rad == c2.rad), d > c1.rad + c2.rad)
        return
    a = (c1.rad_sq - c2.rad_sq + d**2)/(2*d)
    p2 = c1.pos + a*dr/d
    p31 = [0, 0]
    p32 = [0, 0]
    h = (c1.rad_sq - a**2)**0.5
    q1 = h*(c2.pos[1] - c1.pos[1])/d
    q2 = h*(c2.pos[0] - c1.pos[0])/d
    p31[0] = p2[0] + q1
    p31[1] = p2[1] - q2
    p32[0] = p2[0] - q1
    p32[1] = p2[1] + q2
    return p31, p32, a, d


def ssd(arr):
    #sum of square differences
    return np.sum(np.square(arr))

class WorldLawsEngine:

    def __init__(self, display=None, display_size=None):
        self.time_step = 0.02
        self.pixel_to_unit = 1.0
        self.time_to_update = 0.0
        self.env_resistance = 0.01
        self.bounce_dumping = 0.01
        self.quad_tree = QuadTree(0, np.array([0, 0]), np.array(display_size), 10)
        offset = 200
        pos = (display_size[0]/2 - offset, display_size[1]/2)
        self.border = display_size
        rad = 50
        self.p31 = None

        self.springs = {}
        #TODO make ordered dict
        self.cells = []
        """
        n = 6
        rad = 40
        for i in range(n):
            a = 2*pi/n*i
            self.cells.append(Cell([cos(a)*rad + display_size[0]/2, sin(a)*rad + display_size[1]/2],
                [0, 400], 15, 100))
            self.cells[-1].set_add_params(self.time_step, self.pixel_to_unit, self.env_resistance,
                    self.bounce_dumping)
        for c in range(len(self.cells) - 1):
            self.add_spring(self.cells[c], self.cells[c + 1])
        self.add_spring(self.cells[0], self.cells[-1])
        self.add_spring(self.cells[-4], self.cells[-1])
        self.add_spring(self.cells[0], self.cells[-3])
        self.add_spring(self.cells[1], self.cells[-2])
        """
        self.cells.append(Cell([display_size[0]/2 - 100, display_size[1]/2], [60, 0], 50, 10))
        self.cells.append(Cell([display_size[0]/2 + 100, display_size[1]/2+40], [-60, 0], 40, 8))
        #self.add_spring(self.cells[0], self.cells[1], 10, 100)
        for cell in self.cells:
            cell.set_add_params(self.time_step, self.pixel_to_unit, self.env_resistance,
                    self.bounce_dumping)
        self.display = display
        self.display_size = display_size
    #@profile
    def update_state(self):
        self.tick()

    def tick(self):
        self.enforce_boundaries()
        self.update_springs()
        self.update_cells_pos()
        self.process_collisions()

    def find_cell_by_point(self, pos):
        pos = np.array(pos)
        for cell in self.cells:
            if ssd(pos - cell.pos) < cell.rad**2:
                return cell
        return None

    def enforce_boundaries(self):
        for cell in self.cells:
            if cell.pos[0] + cell.rad > self.border[0]:
                cell.vel[0] *= -(1 - self.bounce_dumping)
            elif cell.pos[0] - cell.rad < 0:
                cell.vel[0] *= -(1 - self.bounce_dumping)
                cell.pos[0] = 0 + cell.rad + 0.01

            if cell.pos[1] + cell.rad > self.border[1]:
                cell.vel[1] *= -(1 - self.bounce_dumping)
                cell.pos[1] = self.border[1] - cell.rad - 0.01
            elif cell.pos[1] - cell.rad < 0:
                cell.vel[1] *= -(1 - self.bounce_dumping)
                cell.pos[1] = 0 + cell.rad + 0.01

    def update_springs(self):
        for spring in self.springs.values():
            spring.update_state()

    def add_spring(self, c1, c2, strength=10, eq_len=None):
        if ord_pair(c1, c2) in self.springs:
            #print('check add spring')
            return
        self.springs[ord_pair(c1, c2)] = Spring(c1, c2, strength, eq_len)

    def process_collisions(self):
        quad = True
        collisions = {}
        #if not quad:
        #    for c1 in range(len(self.cells) - 1):
        #        for c2 in range(c1 + 1, len(self.cells)):
        #            if self.cells[c1].cell_collision(self.cells[c2]):
        #                collisions[ord_pair(self.cells[c1], self.cells[c2])] = None
        #if quad:
        for cell in self.cells:
            self.quad_tree.insert(cell)

        for cell in self.cells:
            potentials = []
            self.quad_tree.retrieve(potentials, cell)
            for pot in potentials:
                if cell.id == pot.id or ord_pair(cell, pot) in collisions:
                    continue
                if cell.cell_collision(pot):
                    collisions[ord_pair(cell, pot)] = None

        self.quad_tree.clear()

        for c1, c2 in collisions:
            self.add_spring(c1, c2, 10, c1.rad + c2.rad)
            dr = c2.pos - c1.pos
            d = la.norm(dr)
            if d < abs(c1.rad - c2.rad) or (d == 0 and c1.rad == c2.rad):
                return
            """
            r = c1
            R = c2
            if c1.pos[0] < c2.pos[0]:
                r, R = R, r
            #TODO store squares
            self.x = d**2 - r.rad**2 + R.rad**2
            self.x /= 2*d
            self.x += R.pos[0]
            r = r.rad
            R = R.rad
            self.y = 0.5/d*((-d + r - R)*(-d - r + R)*(-d + r + R)*(d + r + R))**0.5
            self.yy = c1.pos[1]
            """
            a = (c1.rad**2 - c2.rad**2 + d**2)/(2*d)
            p2 = c1.pos + a*dr/d
            self.p31 = [0, 0]
            self.p32 = [0, 0]
            h = (c1.rad**2 - a**2)**0.5
            self.p31[0] = p2[0] + h*(c2.pos[1] - c1.pos[1])/d
            self.p31[1] = p2[1] - h*(c2.pos[0] - c1.pos[0])/d
            self.p32[0] = p2[0] - h*(c2.pos[1] - c1.pos[1])/d
            self.p32[1] = p2[1] + h*(c2.pos[0] - c1.pos[0])/d
            #c1.collide(c2)

    def draw(self):
        for cell in self.cells:
            cell.draw(self.display)
        if self.p31:
            pg.draw.line(self.display, (0,0,0), self.p31, self.p32, 9)
            self.p31 = None

        for spring in self.springs.values():
            spring.draw(self.display)

    def update_cells_pos(self):
        for cell in self.cells:
            cell.update_pos()



class Spring:

    def __init__(self, cell1, cell2, strength=0.01, eq_len=None):
        self.c1 = cell1
        self.c2 = cell2
        #TODO implemen adhesivness like smaller length?
        if eq_len is None:
            self.eq_len = cell1.rad + cell2.rad
            self.eq_len = la.norm(cell1.pos - cell2.pos)
        else:
            self.eq_len = eq_len
        #self.eq_len = la.norm(cell1.pos - cell2.pos) + 1
        self.strength = strength
        self.strength = 100

    def update_state(self):
        dr = self.c1.pos - self.c2.pos
        dist = la.norm(dr)
        force = self.eq_len - dist
        if abs(force) < 0.1:
            return
        force *= self.strength
        direc = dr/dist
        self.c1.vel = self.c1.vel + direc*force/self.c1.mass
        self.c2.vel = self.c2.vel - direc*force/self.c2.mass

    def draw(self, display):
        pg.draw.aaline(display, (0,0,0), self.c1.draw_pos, self.c2.draw_pos)


class Cell:

    def __init__(self, pos, vel, rad, mass):
        self.pos = np.array(pos).astype(float)
        self.rad = rad
        self.rad_sq = rad**2
        self.vel = np.array(vel).astype(float)
        self.mass = mass
        self.acc = np.array([0, 0]).astype(float)
        self.id = next(count)
        self.time_step = None
        self.pixel_to_unit = None
        self.rect_size = np.array([2*rad, 2*rad]).astype(int)
        self.env_resistance = None
        self.bounce_dumping = None

    def set_add_params(self, time_step, pixel_to_unit, env_resistance, bounce_dumping):
        self.time_step = time_step
        self.pixel_to_unit = pixel_to_unit
        self.env_resistance = env_resistance
        self.bounce_dumping = bounce_dumping

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    @property
    def draw_pos(self):
        return (self.pos*self.pixel_to_unit).astype(int)

    def top_left(self):
        return (self.pos - self.rad).astype(int)

    def draw(self, display):
        pg.draw.circle(display, pg.Color('red'), self.draw_pos, self.rad, 7)
        pg.draw.circle(display, pg.Color('green'), self.draw_pos, self.rad - 6)

    def cell_collision(self, cell):
        return ssd(self.pos - cell.pos) < (self.rad + cell.rad)**2

    def collide(self, cell):
        dr = self.pos - cell.pos
        dv = self.vel - cell.vel
        dot = dr.dot(dv.T)
        if not dot < 0:
            return
        dist = la.norm(dr)
        j = 2*self.mass*cell.mass*dot/dist/(self.mass + cell.mass)
        jj = j*dr/dist

        self.pos -= self.vel*self.time_step
        cell.pos -= cell.vel*cell.time_step
        self.vel -= jj/self.mass
        cell.vel += jj/cell.mass
        self.vel *= (1.0 - self.bounce_dumping)
        cell.vel *= (1.0 - self.bounce_dumping)

    def update_pos(self):
        if abs(self.vel[0]) < 0.01 and abs(self.vel[1]) < 0.001:
            self.vel[0] = 0.0
            self.vel[1] = 0.0
            return
        print(self.vel)
        self.pos += self.vel*self.time_step
        self.vel += self.acc*self.time_step
        self.vel *= 1.0 - self.env_resistance

