import pygame as pg
import numpy as np
from numpy import linalg as la
import itertools
from math import atan2, pi, sin, cos, ceil
import time
from quad_tree import QuadTree
import pantograph
#from profilehooks import profile
count = itertools.count()
def ord_pair(q1, q2):
    if q1.id > q2.id:
        return q2, q1
    else:
        return q1, q2

def ssd(arr):
    #sum of square differences
    return np.sum(np.square(arr))

class WorldLawsEngine:

    def __init__(self, display=None, display_size=None):
        self.time_step = 0.02
        self.pixel_to_unit = 1.0
        self.border = (np.array([0.0, 0.0]), display_size)
        self.time_to_update = 0.0
        self.env_resistance = 0.001
        self.bounce_dumping = 0.03
        #TODO
        self.iteration_depth = 1
        self.quad_tree = QuadTree(0, np.array([0, 0]), np.array(display_size))
        offset = 200
        pos = (display_size[0]/2 - offset, display_size[1]/2)
        rad = 50

        self.springs = {}
        #TODO make ordered dict
        self.cells = []
        #self.cells.append(Cell([display_size[0]/2 - 100, display_size[1]/2], [0, 0], 3, 100))
        #self.cells.append(Cell([display_size[0]/2 + 100, display_size[1]/2], [0, 0], 3, 100))
        self.n = 20
        n = self.n
        offset = 90
        h = 30
        for j in range(h):
            for i in range(n):
                self.cells.append(Cell([display_size[0]/2 - offset + offset*2/n*i, 50 + j*offset*2/n], [0, 0], 3, 100))
        for j in range(h - 1):
            for i in range(n - 1):
                self.add_spring(self.cells[i + n*j], self.cells[i + 1 + n*j], 1)
                self.add_spring(self.cells[i + n*j], self.cells[i + n*(j+1)], 1)
            self.add_spring(self.cells[i + 1 + n*j], self.cells[i + 1 + n*(j+1)], 1)
        for i in range(n - 1):
            self.add_spring(self.cells[i + n*(h - 1)], self.cells[i + 1 + n*(h - 1)], 1)
        for cell in self.cells:
            cell.set_add_params(self.time_step, self.pixel_to_unit, self.env_resistance,
                        self.bounce_dumping)

        #self.add_spring(self.cells[0], self.cells[1], 1)
        """
        n = 30
        rad = 200
        for i in range(n):
            a = 2*pi/n*i
            self.cells.append(Cell([cos(a)*rad + display_size[0]/2, sin(a)*rad + display_size[1]/2],
                [0, 400], 15, 100))
            self.cells[-1].set_add_params(self.time_step, self.pixel_to_unit, self.env_resistance,
                    self.bounce_dumping)
        for c in range(len(self.cells) - 1):
            self.add_spring(self.cells[c], self.cells[c + 1])
        self.add_spring(self.cells[0], self.cells[-1])
        """
        self.display = display
        self.display_size = display_size
    #@profile
    def update_state(self):
        self.tick()
        self.draw()

    def tick(self):
        #self.enforce_boundaries()
        self.update_cells_pos()
        self.satisfy_constraints()
        #self.process_collisions()
        #self.update_springs()

    def satisfy_constraints(self):
        """
        The number of necessary iterations varies depending on the physical system simulated and the amount of motion.
        It can be made adaptive by measuring the change from last iteration. If we stop the iterations early,
        the result might not end up being quite valid but because of the Verlet scheme,
        in next frame it will probably be better, next frame even more so etc.
        This means that stopping early will not ruin everything although the resulting animation might appear somewhat sloppier.
        """
        for i in range(self.iteration_depth):
            #TODO maybe check if nothing changes - break the loop
            for cell in self.cells:
                cell.pos = np.minimum(np.maximum(cell.pos, self.border[0]), self.border[1])
            for spring in self.springs.values():
                spring.satisfy_constraints()
            for i in range(self.n):
                self.cells[i].pos[1] = 0

    def find_cell_by_point(self, pos):
        pos = np.array(pos)
        for cell in self.cells:
            if ssd(pos - cell.pos) < cell.rad**2:
                return cell
        return None

    def enforce_boundaries(self):
        for cell in self.cells:
            if cell.pos[0] + cell.rad > self.border[1][0]:
                cell.vel[0] *= -(1 - self.bounce_dumping)
                cell.pos[0] = self.border[1][0] - cell.rad - 0.01
            elif cell.pos[0] - cell.rad < self.border[0][0]:
                cell.vel[0] *= -(1 - self.bounce_dumping)
                cell.pos[0] = self.border[0][0] + cell.rad + 0.01

            if cell.pos[1] + cell.rad > self.border[1][1]:
                cell.vel[1] *= -(1 - self.bounce_dumping)
                cell.pos[1] = self.border[1][1] - cell.rad - 0.01
            elif cell.pos[1] - cell.rad < self.border[0][1]:
                cell.vel[1] *= -(1 - self.bounce_dumping)
                cell.pos[1] = self.border[0][1] + cell.rad + 0.01

    def update_springs(self):
        for spring in self.springs.values():
            spring.update_state()

    def add_spring(self, c1, c2, strength=0.5, eq_len=None):
        if ord_pair(c1, c2) in self.springs:
            print('check add spring')
            return
        self.springs[ord_pair(c1, c2)] = Spring(c1, c2, self.time_step, strength, eq_len=None)

    def process_collisions(self):
        quad = True
        collisions = {}
        #if not quad:
        #    for c1 in range(len(self.cells) - 1):
        #        for c2 in range(c1 + 1, len(self.cells)):
        #            if self.cells[c1].cell_collision(self.cells[c2]):
        #                collisions[ord_pair(self.cells[c1], self.cells[c2])] = None
        #if quad:
        for cell in self.cells:
            self.quad_tree.insert(cell)

        for cell in self.cells:
            potentials = []
            self.quad_tree.retrieve(potentials, cell)
            for pot in potentials:
                if cell.id == pot.id or ord_pair(cell, pot) in collisions:
                    continue
                if cell.cell_collision(pot):
                    collisions[ord_pair(cell, pot)] = None

        #self.quad_tree.root_clear()
        self.quad_tree.clear()

        for c1, c2 in collisions:
            c1.collide(c2)

    def draw(self):
        for cell in self.cells:
            cell.draw(self.display)
        for spring in self.springs.values():
            spring.draw(self.display)

    def update_cells_pos(self):
        for cell in self.cells:
            cell.update_pos()



class Spring:

    def __init__(self, cell1, cell2, time_step, strength=0.01, eq_len=None):
        self.c1 = cell1
        self.c2 = cell2
        #TODO implemen adhesivness like smaller length?
        if eq_len is None:
            self.eq_len = la.norm(cell1.pos - cell2.pos)
        else:
            self.eq_len = eq_len
        #self.eq_len = la.norm(cell1.pos - cell2.pos) + 1
        self.strength = strength
        self.eq_len_sq = self.eq_len**2
        self.time_step = time_step/time_step
        self.drawable = pantograph.Line(cell1.pos[0], cell1.pos[1], cell2.pos[0], cell2.pos[1],
                                                "#0f0")

    def satisfy_constraints(self):
        dr = self.c1.pos - self.c2.pos
        #dist = la.norm(dr)
        #diff = (dist - self.eq_len)/dist
        #self.c1.pos -= dr*0.5*diff*self.time_step*self.strength
        #self.c2.pos += dr*0.5*diff*self.time_step*self.strength
        #rough approximation
        #TODO dr*dr WAS DOT  PRODUCt LOL
        dr *= self.eq_len_sq/(dr[0]*dr[0]+dr[1]*dr[1]+self.eq_len_sq)-0.5
        self.c1.pos += dr
        self.c2.pos -= dr

    def draw(self, display):
        #pg.draw.aaline(display, (0,0,0), self.c1.draw_pos, self.c2.draw_pos)
        self.drawable.startx = self.c1.pos[0]
        self.drawable.starty = self.c1.pos[1]
        self.drawable.endx = self.c2.pos[0]
        self.drawable.endy = self.c2.pos[1]
        self.drawable.draw(display)


class Cell:

    def __init__(self, pos, vel, rad, mass):
        self.pos = np.array(pos).astype(float)
        self.rad = rad
        self.prev_pos = self.pos - vel
        self.mass = mass
        self.acc = np.array([0, 0]).astype(float)
        self.id = next(count)
        self.time_step = None
        self.pixel_to_unit = None
        self.rect_size = np.array([2*rad, 2*rad]).astype(int)
        self.env_resistance = None
        self.bounce_dumping = None
        #self.gravity = np.array([0, 9.8])
        self.gravity = np.array([0, 0.01])
        self.acc = self.gravity
        self.drawable = pantograph.Circle(pos[0], pos[1], rad, "#0f0")

    def set_add_params(self, time_step, pixel_to_unit, env_resistance, bounce_dumping):
        #TODO LOL
        self.time_step = 1
        self.time_step_sq = 1
        #self.time_step = time_step
        #self.time_step_sq = time_step**2
        self.pixel_to_unit = pixel_to_unit
        self.env_resistance = env_resistance
        self.bounce_dumping = bounce_dumping

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    @property
    def draw_pos(self):
        q = self.pos*self.pixel_to_unit
        return int(q[0]), int(q[1])

    def top_left(self):
        return np.rint((self.pos - self.rad))

    def draw(self, display):
        #pg.draw.circle(display, pg.Color('red'), self.draw_pos, self.rad, 1)
        #pg.draw.circle(display, pg.Color('green'), self.draw_pos, self.rad - 1)
        #pg.draw.circle(display, (0, 0, 0), self.draw_pos, 2)
        self.drawable.x = self.pos[0]
        self.drawable.y = self.pos[1]
        self.drawable.draw(display)

    def cell_collision(self, cell):
        return ssd(self.pos - cell.pos) < (self.rad + cell.rad)**2

    def collide(self, cell):
        dr = self.pos - cell.pos
        dv = self.vel - cell.vel
        dot = dr.dot(dv.T)
        if not dot < 0:
            return
        dist = la.norm(dr)
        j = 2*self.mass*cell.mass*dot/dist/(self.mass + cell.mass)
        jj = j*dr/dist

        self.pos -= self.vel*self.time_step
        cell.pos -= cell.vel*cell.time_step
        self.vel -= jj/self.mass
        cell.vel += jj/cell.mass
        self.vel *= (1.0 - self.bounce_dumping)
        cell.vel *= (1.0 - self.bounce_dumping)

    def update_pos(self):
        self.accumulate_forces()
        #TODO replace all 1 - coeff with new_coeff*
        self.pos, self.prev_pos = self.pos + (1 - self.env_resistance)*(self.pos - self.prev_pos) + self.acc*self.time_step_sq, self.pos

    def accumulate_forces(self):
        #self.acc = self.gravity
        pass

