#include "Object.h"


Object::Object( int _id, float _x, float _y, float _width, float _height ) :
  id (_id),
  x   ( _x ),
  y   ( _y ),
  width ( _width ),
  height  ( _height )
{
}
