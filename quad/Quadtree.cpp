//#include "Circle.h"
#include <Python.h>
#include <vector>
#include <cstddef>
#include <iostream>
#include <stdio.h>

using namespace std;

static const int max_obj = 5;
static const int max_lvl = 5;
class Quadtree;
class Object;
class Object {
public:
  int id;
  float         x;
  float         y;
  float         width;
  float         height;

              Object( int id, float x, float y, float width, float height );
};

Object::Object( int _id, float _x, float _y, float _width, float _height ) :
  id (_id),
  x   ( _x ),
  y   ( _y ),
  width ( _width ),
  height  ( _height )
{
}

class Quadtree {
public:
            Quadtree(float x, float y, float width, float height, int level);

            ~Quadtree();

  void          Insert(Object *object);
  //void Retrieve(Vector *return_objects, *obj);
  void GetAllQuads(PyObject *lst);
  void          Clear();
  void          Split();
  int          GetLen();
  int GetIndex(Object *obj);

private:
  float         x;
  float         y;
  int len;
  float         width;
  float         height;
  int         level;
  float hh;
  float hw;
  float cx;
  float cy;
  vector<Object*>       objects;

  Quadtree *nodes[4];

};
Quadtree::Quadtree(float _x, float _y, float _width, float _height, int _level) :
  x   (_x),
  y   (_y),
  width (_width),
  height  (_height),
  level (_level),
  hw (width / 2.0f),
  hh (height / 2.0f),
  cx (x + hw),
  cy (y + hh)
{
  len = 0;
  nodes[0] = NULL;
  nodes[1] = NULL;
  nodes[2] = NULL;
  nodes[3] = NULL;
}

void Quadtree::Split()
{

  nodes[0] = new Quadtree(x, y, hw, hh, level+1);
  nodes[1] = new Quadtree(x + hw, y, hw, hh, level+1);
  nodes[2] = new Quadtree(x, y + hh, hw, hh, level+1);
  nodes[3] = new Quadtree(x + hw, y + hh, hw, hh, level+1);
}

Quadtree::~Quadtree()
{
  if (level == max_lvl)
    return;

  delete nodes[0];
  delete nodes[1];
  delete nodes[2];
  delete nodes[3];
}
void Quadtree::Insert(Object *object) {
  len++;
  if (nodes[0] != NULL){
    int index = this->GetIndex(object);

    if (index != -1) {
      nodes[index]->Insert(object);
      return;
    }
  }
  objects.push_back(object);

  if (objects.size() > max_obj && level < max_lvl) {
    if (nodes[0] == NULL){
      this->Split();
    }

    int i = 0;
    while(i < objects.size()){
      int index = this->GetIndex(objects[i]);
      if (index != -1){
        nodes[index]->Insert(objects[i]);
        objects.erase(objects.begin() + i);
      }
      else{
        i++;
      }

    }

  }
}

int Quadtree::GetIndex(Object *obj){
  int  index = -1;
  bool topQ = obj->y < cy;
  bool bottomQ = obj->y > cx;

  if (obj->x + obj->width < cx){
    if (topQ){
      index = 1;
    }
    else if (bottomQ){
      index = 2;
    }
  }
  else if (obj->x > cx){
    if (topQ){
      index = 0;
    }
    else if(bottomQ){
      index = 3;
    }
  }
  return index;
}
void Quadtree::GetAllQuads(PyObject *lst){
  if(!objects.empty()){
      PyObject *l = PyList_New(objects.size());
      for(int i  = 0; i < objects.size(); i++){
        PyList_SetItem(l, i, PyLong_FromLong(objects[i]->id));
      }
      PyList_Append(lst, l);
  }
  if(nodes[0] == NULL){
    return;
  }
  for(int i = 0; i < 4; i++){
    nodes[i]->GetAllQuads(lst);
  }
}
int Quadtree::GetLen(){
  return len;
}


void Quadtree::Clear() {
  if (level == max_lvl || nodes[0] == NULL) {
    objects.clear();
    return;
  } else {
    nodes[0]->Clear();
    nodes[1]->Clear();
    nodes[2]->Clear();
    nodes[3]->Clear();
    nodes[0] = NULL;
    nodes[1] = NULL;
    nodes[2] = NULL;
    nodes[3] = NULL;
  }


  if (!objects.empty()) {
    objects.clear();
  }
}
extern "C" {
      Quadtree* new_Quadtree(float x, float y, float width, float height, int level){
        Quadtree *tree = new Quadtree(x, y, width, height, level);
        return tree;
      }
      PyObject* GetAllQuadLists(Quadtree *tree){
        PyObject *lst = PyList_New(0);
        tree->GetAllQuads(lst);
        return lst;
      }
      void Clear(Quadtree *tree){
        tree->Clear();
      }
      void Insert(Quadtree *tree, float x, float y, float w, float h){ }
      void InsertCircle(Quadtree *tree, int id, float cx, float cy, float rad){
        Object *circle = new Object(id, cx - rad, cy - rad, rad*2, rad*2);
        tree->Insert(circle);
      }
}
