//#include "Circle.h"
#include <vector>
#include <cstddef>
#include <iostream>
#include <stdio.h>

using namespace std;

static const int max_obj = 5;
static const int max_lvl = 5;
class Quadtree;
class Object;
class Object {
public:
  int id;
  float         x;
  float         y;
  float         width;
  float         height;

              Object( int id, float x, float y, float width, float height );
};

Object::Object( int _id, float _x, float _y, float _width, float _height ) :
  id (_id),
  x   ( _x ),
  y   ( _y ),
  width ( _width ),
  height  ( _height )
{
}

class Quadtree {
public:
            Quadtree(float x, float y, float width, float height, int level);

            ~Quadtree();

  void          Insert(Object *object);
  void InsertCircle(int id, float cx, float cy, float rad);

private:
  float         x;
  float         y;
  int len;
  float         width;
  float         height;
  int         level;
  float hh;
  float hw;
  float cx;
  float cy;
  vector<Object*>       objects;

  Quadtree *nodes[4];

};
Quadtree::Quadtree(float _x, float _y, float _width, float _height, int _level) :
  x   (_x),
  y   (_y),
  width (_width),
  height  (_height),
  level (_level),
  hw (width / 2.0f),
  hh (height / 2.0f),
  cx (x + hw),
  cy (y + hh)
{
  len = 0;
  cout << "Constructor\n";
}

Quadtree::~Quadtree()
{
  if (level == max_lvl)
    return;

  delete nodes[0];
  delete nodes[1];
  delete nodes[2];
  delete nodes[3];
}
void Quadtree::Insert(Object *object) {
  cout << "\n****\n";
  //len++;
  cout << x << "\n";
  //cout << (nodes[0] == NULL);
  cout << "\n****\n";
}

extern "C" {
      Quadtree* new_Quadtree(float x, float y, float width, float height, int level){
        cout << "Create\n";
        Quadtree *tree = new Quadtree(x, y, width, height, level);
        return tree;
      }
      void Insert(Quadtree *tree, float x, float y, float w, float h){
        tree->Insert(new Object(0, 0, 0, 0, 0));
      }
}
