#from ctypes import CDLL, py_object, RTLD_GLOBAL
from ctypes import *

dll = CDLL('./libquad.so', mode=RTLD_GLOBAL)
print(1)
dll.new_Quadtree.argtypes = [c_float, c_float, c_float, c_float, c_int]
dll.new_Quadtree.restype = c_size_t
quad_tree = dll.new_Quadtree(0, 0, 100, 100, 0)
print(2)
dll.Insert.argtypes = [c_size_t, c_int, c_float, c_float, c_float]
dll.Insert.restype = c_void_p
dll.Insert(quad_tree, 0, 10, 10, 3)
