#include "Circle.h"


Circle::Circle( int _id, float _x, float _y, float _rad) :
  Object::Object(_id, _x - _rad, _y - _rad, _rad*2, _rad*2)
{
}
