#ifndef __QUADTREE_H__
#define __QUADTREE_H__

#include <vector>
#include <Python.h>

using namespace std;

class Quadtree;
class Object;

class Quadtree {
public:
            Quadtree(float x, float y, float width, float height, int level);

            ~Quadtree();

  void          Insert(Object *object);
  //void Retrieve(Vector *return_objects, *obj);
  void GetAllQuads(PyObject *lst);
  void InsertCircle(int id, float cx, float cy, float rad);
  void          Clear();
  void          Split();
  int          GetLen();
  int GetIndex(Object *obj);

private:
  float         x;
  float         y;
  int len;
  float         width;
  float         height;
  int         level;
  float hh;
  float hw;
  float cx;
  float cy;
  vector<Object*>       objects;

  Quadtree *nodes[4];

};

#endif
