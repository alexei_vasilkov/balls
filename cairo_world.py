"""
Based on cairo-demo/X11/cairo-demo.c
"""

from gi.repository import Gtk
from gi.repository import Gdk
import time
from gi.repository import GObject as gobject
from math import pi
#from test_verlet import WorldLawsEngine
from verlet_imp import WorldLawsEngine
from cairo_primitives import message
import random
import numpy as np
from profilehooks import profile

SIZE = 20
WIN_SIZE = 800, 600
PI2 = pi*2


class Win(Gtk.Window):

    def __init__(self, size=(800, 600)):
        super(Win, self).__init__()

        self.init_ui(size)

    def init_ui(self, size):

        self.darea = Gtk.DrawingArea()
        self.darea.connect("draw", self.redraw)
        self.add(self.darea)

        self.set_title("Much balls.")
        self.resize(size[0], size[1])
        self.set_position(Gtk.WindowPosition.CENTER)
        self.connect("delete-event", Gtk.main_quit)
        self.connect('destroy', lambda w: Gtk.main_quit())
        self.connect('key_press_event', self.key_pressed)
        self.connect('button_press_event', self.mouse_click)
        self.show_all()
        self.pause = True
        self.last = time.time()
        self.engine = WorldLawsEngine(size)
        gobject.timeout_add(20, self.tick) # Go call tick every 50 whatsits.
        self.q = 0

    def key_pressed(self, event, event2):
        if event2.keyval == ord('p'):
            self.pause = not self.pause

    def mouse_click(self, event, mouse):
        #print(self.engine.t)
        r = random.random()*10 + 20
        pos = mouse.get_coords()
        cell = self.engine.find_cell_by_point(pos, r)
        if cell is None:
            self.engine.add_cell(pos, np.random.rand(2)*8 - 4, r, 8)

    #@profile
    def redraw(self, wid, ctx):
        # paint background
        ctx.set_source_rgb(1, 1, 1) # blue
        ctx.rectangle(0, 0, WIN_SIZE[0], WIN_SIZE[1])
        ctx.fill()

        #self.circle(ctx, [300, 300], 30, w=10)
        #self.line(ctx, [0,0],[500,500])
        self.engine.draw(ctx)
        message(ctx, (20, 40), 'fps: %0.1f' % (1/(time.time() - self.last)))
        message(ctx, (20, 80), str(len(self.engine.cells)))
        message(ctx, (20, 120), str(len(self.engine.connections)))
        self.last = time.time()

    #@profile
    def tick(self):
        if self.pause:
            return True
        self.q += 1
        if self.q > 2:
            self.q = 0
            #if len(self.engine.cells) < 150:
            #    self.engine.add_cell((300, 300), np.random.rand(2)*10 - 5, 10, 3)
        self.engine.update_state()
        self.darea.queue_draw()
        #IMPORTANT TO RETURN TRUE TO TIMEOUT_ADD
        #Gtk.main_quit()
        return True



def main():
    app = Win()
    #Gtk.gtk_window_set_modal(app, True)
    Gtk.main()

if __name__ == '__main__':
    main()

