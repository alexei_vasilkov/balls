def draw_cell(ctx, cell):
    pos = pos
    w = 10
    rad = 70 - w/2
    b_c = (34/255, 139/255, 34/255)
    ctx.set_line_width(w)
    ctx.set_source_rgb(b_c[0], b_c[1], b_c[2])

    ctx.arc_negative(pos[0], pos[1], rad, 0, -pi/2)
    ctx.arc_negative(pos[0], pos[1], rad, pi, pi/2)

    ctx.stroke()
    ctx.set_source_rgb(fill_c[0], fill_c[1], fill_c[2])

    ctx.set_line_width(3)
    ctx.set_source_rgb(0, 0, 0)

    ctx.arc_negative(pos[0], pos[1], rad +w/2 - 3/2, 0, -pi/2)
    ctx.arc_negative(pos[0], pos[1], rad +w/2 - 3/2, pi, pi/2)
    ctx.close_path()

    ctx.stroke()

    ctx.arc_negative(pos[0], pos[1], rad, 0, -pi/2)
    ctx.arc_negative(pos[0], pos[1], rad, pi, pi/2)

    ctx.close_path()

    ctx.stroke()
    draw_spikes(ctx, pos, rad + w/2)


