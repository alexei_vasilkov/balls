from math import ceil, floor
from collections import defaultdict
xx = 0
yy = 0
w = 100
h = 100

class Circle:

    def __init__(self, x, y, rad):
        self.x = x
        self.y = y
        self.rad = rad
        self.array_pos = -1
        self.key = None

    def __str__(self):
        return "%d %d %d %d" % (self.array_pos, self.x, self.y, self.rad)

def get_collisions(self, cells, size):
    #order is important
c1 = Circle(10, 10, 5)
c2 = Circle(10, 20, 5)
c3 = Circle(10, 30, 8)
c1.array_pos = 0
c2.array_pos = 1
c3.array_pos = 2

objects = [c1, c2, c3]
#min rad
cell_size = 1.4*5
cells = defaultdict(list)


for q in objects:
    q.key = (floor(q.x/cell_size), floor(q.y/cell_size))
    cells[q.key].append(q)
print(cells)
for i in range(len(objects)):
    Ri = ceil(2*objects[i].rad / cell_size)
    obj_i = objects[i]
    for x in range(-Ri, Ri + 1):
        for y in range(-Ri, Ri + 1):
            for obj in cells[(q.key[0] + x, q.key[1] + y)]:
                if obj_i.rad > obj.rad or (obj_i.rad == obj.rad and i < obj.array_pos):
                    if (obj_i.x - obj.x)*(obj_i.x - obj.x) + (obj_i.y - obj.y)*(obj_i.y - obj.y) -\
                            (obj_i.rad + obj.rad)*(obj_i.rad + obj.rad) < 0.1:
                                print('Collision', obj_i, obj)

