import pygame as pg
import numpy as np
from numpy import linalg as la
import time
from math import sin, cos, pi, radians as rad
#from test_bot import Cell
#from test_bot import WorldLawsEngine
from test_verlet import WorldLawsEngine
from test_verlet import Cell

#colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
d90 = rad(90)

from profilehooks import profile
class World:

    def __init__(self, ai=None):
        self.ai = ai
        self.init_pygame()
        self.init_world_objects()
        #last command
        self.world_loop()

    def init_world_objects(self):
        #self.cart = Cart(self.fps, self.ai)
        #set network into initial state
        self.in_play = True
        self.exit = False
        self.pause = True
        self.game_over = False
        self.engine = WorldLawsEngine(self.world_display, self.display_size)
        self.selected_cell = None
        self.rects = []
        self.old_rects = []

    def init_pygame(self):

        pg.init()

        self.display_width = 800
        self.display_height = 600

        self.display_size = np.array([self.display_width, self.display_height])
        self.world_display = pg.display.set_mode(self.display_size)#, pg.DOUBLEBUF)
        self.world_display.set_alpha(None)

        pg.display.set_caption('THE BALLS')

        self.fps = 2

        self.clock = pg.time.Clock()
        self.background_color = WHITE


    def choose_action(self, event):
        if event.type == pg.QUIT:
            self.exit = True
            self.in_play = False
        elif event.type == pg.KEYUP:
            if event.key == pg.K_p:
                self.pause = not self.pause
        elif event.type == pg.MOUSEBUTTONDOWN:
            self.selected_cell = self.engine.find_cell_by_point(pg.mouse.get_pos())
            if self.selected_cell:
                self.selected_cell = None
                return
            else:
                self.engine.cells.append(Cell(pg.mouse.get_pos(), np.random.rand(2)*10 - 5, 7, 30))
                #self.engine.cells.append(Cell(pg.mouse.get_pos(), [0, 0], 7, 30))
                self.engine.cells[-1].set_add_params(self.engine.time_step, self.engine.pixel_to_unit,
                    self.engine.env_resistance, self.engine.bounce_dumping)
        #elif event.type == pg.MOUSEBUTTONUP:
            self.selected_cell = None

        #if self.selected_cell:
        #    self.selected_cell.pos = np.array(pg.mouse.get_pos())

    def update_world(self):
        #self.update_car_position()
        #self.check_boundaries()
        if not self.pause:
            self.engine.update_state()

    #@profile
    def redraw_world(self):
        #for rect in self.rects:
        #    self.world_display.fill(self.background_color, rect)
        self.world_display.fill(self.background_color)
        self.engine.draw()
        self.display_game_state('fps: %0.1f' % self.clock.get_fps(), [90, 40])
        if self.pause:
            self.display_game_state('Paused')
        pg.display.update()
        #self.cart.draw(self.world_display)
        #or flip
        #update() updates all, with params updates only it
        #pg.display.update(self.rects + self.old_rects)
        #fps if want fast but smooth movement make over 30
        self.clock.tick(self.fps)

    def display_message(self, msg):
        large_text = pg.font.Font('freesansbold.ttf', 120)

        self.update_current_message(msg, large_text)
        self.current_message[1].center = self.display_size / 2
        self.world_display.blit(*self.current_message)
        pg.display.update()

        time.sleep(1)

    def display_game_state(self, msg, center=None):
        large_text = pg.font.Font('freesansbold.ttf', 30)

        self.update_current_message(msg, large_text)
        if center is None:
            self.current_message[1].center = [self.display_width - 115, 40]
        else:
            self.current_message[1].center = center
        self.world_display.blit(*self.current_message)
        self.rects.append(self.current_message[1])

    def update_current_message(self, msg, font):
        surface = font.render(msg, True, BLACK)
        self.current_message = [surface, surface.get_rect()]

    #@profile
    def world_loop(self):
        self.world_display.fill(self.background_color)
        pg.display.update()
        while self.in_play and not self.exit and not self.game_over:
            #list of events per frame
            for event in pg.event.get():
                self.choose_action(event)
            self.update_world()
            self.redraw_world()
        if self.game_over:
            self.display_message('GAME OVER')
        if self.exit:
            self.quit()
        else:
            pass
            #reset

    def quit(self):
        pg.quit()
        quit()
