from ctypes import CDLL, py_object, RTLD_GLOBAL, c_size_t, c_float, c_int, c_void_p, c_uint64

class QuadTree:

    def __init__(self, x, y, width, height):
        self.lib = CDLL('./libquad.so', mode=RTLD_GLOBAL)

        self.lib.new_Quadtree.argtypes = [c_float, c_float, c_float, c_float, c_int]
        self.lib.new_Quadtree.restype = c_void_p

        self.lib.InsertCircle.argtypes = [c_void_p, c_int, c_float, c_float, c_float]
        self.lib.InsertCircle.restype = c_void_p

        #self.lib.InsertCircle(quad_tree, 3, 10., -40., 3.)

        self.lib.GetAllQuadLists.argtypes = [c_void_p]
        self.lib.GetAllQuadLists.restype = py_object

        #res = self.lib.GetAllQuadLists(quad_tree)

        self.lib.Clear.argtypes = [c_void_p]
        self.lib.Clear.restype = c_void_p
        #self.lib.Clear(quad_tree)
        self.quad_tree = self.lib.new_Quadtree(x, y, width, height, 0)

    def insert_circle(self, _id, x, y, r):
        self.lib.InsertCircle(self.quad_tree, _id, x, y, r)

    def insert_cell(self, cell):
        self.lib.InsertCircle(self.quad_tree, cell.id, cell.pos[0], cell.pos[1], cell.rad)


    def get_quads(self):
        return self.lib.GetAllQuadLists(self.quad_tree)

    def clear(self):
        self.lib.Clear(self.quad_tree)

