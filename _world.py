import pygame as pg
import numpy as np
import time
from math import sin, cos, pi, radians as rad

from double_pole_physics import PoledCart

def get_normilized_dpb_input(dpb):
    return np.array([dpb.cart_pos/dpb.track_limit,
                     #dpb.cart_vel/4.0,
                     dpb.poles[0].angle/dpb.p_failure_angle,
                     #dpb.poles[0].vel/4.0,
                     dpb.poles[1].angle/dpb.p_failure_angle])
                     #dpb.poles[1].vel/4.0])

#colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
d90 = rad(90)
class Cart:

    def __init__(self, fps, ai=None):
        self.sim_engine = PoledCart(2)
        assert 1.0 / fps == self.sim_engine.time_step*2
        #self.sim_engine.cart_pos = 0.5
        self.meter_pixel_ratio = 100
        self.poles_lens = [p.h_len * 2 * self.meter_pixel_ratio for p in self.sim_engine.poles]
        self.cart_color = GREEN
        self.pole_color = RED
        self.pos = [0, 0]
        self.ai = ai
        self.ai_on = False
        #cart size in meters
        cart_size = np.array([1.5, 0.3])
        self.cart_size = cart_size * self.meter_pixel_ratio
        move_force = 5
        self.arrow_fill_color = GREEN
        self.arrow_edge_width = 5
        self.arrow_edge_color = RED
        self.wheel_color = BLACK
        self.wheel_width = 3
        self.wheel_angle = 0.0
        self.wheel_spoke_width = 3
        self.wheel_spoke_color = BLUE
        self.wheel_radius = int(0.2 * self.meter_pixel_ratio)
        self.wheel_len = 2 * pi * self.wheel_radius / self.meter_pixel_ratio
        self.CART_MOVES = {pg.K_LEFT : -move_force,
                           pg.K_RIGHT : move_force
                          }

    def set_pos(self, pos):
        self.pos = pos

    def draw(self, display):

        cart_pos = (self.pos[0] + self.sim_engine.cart_pos*self.meter_pixel_ratio, self.pos[1])

        rect = pg.draw.rect(display, self.cart_color, (cart_pos[0] - self.cart_size[0]/2, cart_pos[1], self.cart_size[0],
                                                self.cart_size[1]), 3)

        #TODO make loop for wheels
        #drawing left wheel
        wheel_pos = (int(cart_pos[0] - self.cart_size[0]/2 + self.wheel_radius*1.5),
                     int(cart_pos[1] + self.cart_size[1] - self.wheel_radius*0.1))
        #first spoke
        pg.draw.line(display, self.wheel_spoke_color,
                     (cos(self.wheel_angle + pi) * (self.wheel_radius - 1) + wheel_pos[0], wheel_pos[1] +
                         sin(self.wheel_angle + pi) * (self.wheel_radius - 1)),
                     (cos(self.wheel_angle) * (self.wheel_radius - 1) + wheel_pos[0], wheel_pos[1] + sin(self.wheel_angle) * (self.wheel_radius - 1)),
                     self.wheel_spoke_width)

        #second spoke
        pg.draw.line(display, self.wheel_spoke_color,
                     (cos(self.wheel_angle + 1.5*pi) * (self.wheel_radius - 1) + wheel_pos[0], wheel_pos[1] +
                         sin(self.wheel_angle + 1.5*pi) * (self.wheel_radius - 1)),
                     (cos(self.wheel_angle + 0.5*pi) * (self.wheel_radius - 1) + wheel_pos[0],
                         wheel_pos[1] + sin(self.wheel_angle + 0.5*pi) * (self.wheel_radius - 1)),
                     self.wheel_spoke_width)

        pg.draw.circle(display, self.wheel_color, wheel_pos, self.wheel_radius, self.wheel_width)

        #drawing right wheel
        wheel_pos = (int(cart_pos[0] + self.cart_size[0]/2 - self.wheel_radius*1.5),
                     int(cart_pos[1] + self.cart_size[1] - self.wheel_radius*0.1))
        pg.draw.line(display, self.wheel_spoke_color,
                     (cos(self.wheel_angle + pi) * (self.wheel_radius - 1) + wheel_pos[0], wheel_pos[1] +
                         sin(self.wheel_angle + pi) * (self.wheel_radius - 1)),
                     (cos(self.wheel_angle) * (self.wheel_radius - 1) + wheel_pos[0], wheel_pos[1] + sin(self.wheel_angle) * (self.wheel_radius - 1)),
                     self.wheel_spoke_width)

        pg.draw.line(display, self.wheel_spoke_color,
                     (cos(self.wheel_angle + 1.5*pi) * (self.wheel_radius - 1) + wheel_pos[0], wheel_pos[1] +
                         sin(self.wheel_angle + 1.5*pi) * (self.wheel_radius - 1)),
                     (cos(self.wheel_angle + 0.5*pi) * (self.wheel_radius - 1) + wheel_pos[0],
                         wheel_pos[1] + sin(self.wheel_angle + 0.5*pi) * (self.wheel_radius - 1)),
                     self.wheel_spoke_width)
        pg.draw.circle(display, self.wheel_color, wheel_pos, self.wheel_radius, self.wheel_width)

        #drawing arrows
        arrow_w = 60
        arrow_h = 40
        x = display.get_width() * 0.5 + 30
        y = display.get_height() - arrow_h - 30
        if self.sim_engine.applied_force > 0:
            pg.draw.polygon(display, self.arrow_fill_color, ((x, y + arrow_h/3), (x, y + arrow_h*2/3),
                (x + arrow_w*2/3, y + arrow_h*2/3), (x + arrow_w*2/3, y + arrow_h),
                (x + arrow_w, y + arrow_h/2),
                (x + arrow_w*2/3, y), (x + arrow_w*2/3, y + arrow_h/3)))

        pg.draw.polygon(display, self.arrow_edge_color, ((x, y + arrow_h/3), (x, y + arrow_h*2/3),
            (x + arrow_w*2/3, y + arrow_h*2/3), (x + arrow_w*2/3, y + arrow_h),
            (x + arrow_w, y + arrow_h/2),
            (x + arrow_w*2/3, y), (x + arrow_w*2/3, y + arrow_h/3)), 5)

        x = display.get_width() * 0.5 - 30
        y = display.get_height() - arrow_h - 30


        if self.sim_engine.applied_force < 0:
            pg.draw.polygon(display, self.arrow_fill_color, ((x, y + arrow_h/3), (x, y + arrow_h*2/3),
                (x - arrow_w*2/3, y + arrow_h*2/3), (x - arrow_w*2/3, y + arrow_h),
                (x - arrow_w, y + arrow_h/2),
                (x - arrow_w*2/3, y), (x - arrow_w*2/3, y + arrow_h/3)))

        pg.draw.polygon(display, self.arrow_edge_color, ((x, y + arrow_h/3), (x, y + arrow_h*2/3),
            (x - arrow_w*2/3, y + arrow_h*2/3), (x - arrow_w*2/3, y + arrow_h),
            (x - arrow_w, y + arrow_h/2),
            (x - arrow_w*2/3, y), (x - arrow_w*2/3, y + arrow_h/3)), 5)



        for i in range(self.sim_engine.pole_number):
            angle = self.sim_engine.poles[i].angle + d90
            end_pos = np.array(cart_pos) + self.poles_lens[i]*np.array([cos(angle),
                -sin(angle)])
            pg.draw.line(display, self.pole_color, cart_pos, end_pos, 3)

    def update_position(self):
        self.sim_engine.applied_force = 0.0
        if self.ai is not None and self.ai_on:
            self.sim_engine.applied_force = (self.ai.feed(get_normilized_dpb_input(self.sim_engine))[0])*10
        for key in self.controls:
            if self.controls[key]:
                self.sim_engine.applied_force = self.CART_MOVES[key]
        self.sim_engine.update_state()
        self.sim_engine.update_state()
        self.wheel_angle = self.sim_engine.cart_pos / self.wheel_len * pi / 2



class World:

    def __init__(self, ai=None):
        self.ai = ai
        self.init_pygame()
        self.init_world_objects()

        #last command
        self.world_loop()

    def init_world_objects(self):
        self.cart = Cart(self.fps, self.ai)
        #set network into initial state
        if self.ai is not None:
            self.ai.reinit_network()

        self.in_play = True
        self.exit = False
        self.game_over = False

        self.cart.set_pos(self.display_size/2)

        self.cart.controls = {pg.K_LEFT : False,
                        pg.K_RIGHT : False,
                       }



    def init_pygame(self):

        pg.init()

        self.display_width = 800
        self.display_height = 600

        self.display_size = np.array([self.display_width, self.display_height])
        self.world_display = pg.display.set_mode(self.display_size)

        pg.display.set_caption('A bit Racey')

        self.fps = 50

        self.clock = pg.time.Clock()
        self.background_color = WHITE


    def choose_action(self, event):
        if event.type == pg.QUIT:
            self.exit = True
            self.in_play = False
        if event.type == pg.KEYDOWN:
            if event.key in self.cart.controls:
                self.cart.controls[event.key] = True
            elif event.key == pg.K_ESCAPE:
                self.init_world_objects()
            elif event.key == pg.K_a:
                if self.ai is not None:
                    ai_on = self.cart.ai_on
                    self.init_world_objects()
                    self.cart.ai_on = not ai_on
        if event.type == pg.KEYUP:
            if event.key in self.cart.controls:
                self.cart.controls[event.key] = False

    def update_world(self):
        self.update_car_position()
        #self.check_boundaries()

    def check_boundaries(self):
        for i in range(len(self.car_pos)):
            if self.car_pos[i] < 0:
                self.car_pos[i] = 0
                self.game_over = True
            elif self.car_pos[i] + self.car_size[i] > self.display_size[i]:
                self.car_pos[i] = self.display_size[i] - self.car_size[i]
                self.game_over = True

    def update_car_position(self):
        self.cart.update_position()

    def redraw_world(self):
        self.world_display.fill(self.background_color)
        self.cart.draw(self.world_display)
        #or flip
        #update() updates all, with params updates only it
        if self.cart.ai_on and self.ai is not None:
            self.display_game_state('AI NEAT')
        else:
            self.display_game_state('Human me')

        pg.display.update()
        #fps if want fast but smooth movement make over 30
        self.clock.tick(self.fps)

    def display_message(self, msg):
        large_text = pg.font.Font('freesansbold.ttf', 120)

        self.update_current_message(msg, large_text)
        self.current_message[1].center = self.display_size / 2
        self.world_display.blit(*self.current_message)
        pg.display.update()

        time.sleep(1)

    def display_game_state(self, msg):
        large_text = pg.font.Font('freesansbold.ttf', 30)

        self.update_current_message(msg, large_text)
        self.current_message[1].center = [self.display_width - 115, 40]
        self.world_display.blit(*self.current_message)
        #pg.display.update()

        #time.sleep(1)

    def update_current_message(self, msg, font):
        surface = font.render(msg, True, BLACK)
        self.current_message = [surface, surface.get_rect()]


    def world_loop(self):
        while self.in_play and not self.exit and not self.game_over:
            #list of events per frame
            for event in pg.event.get():
                self.choose_action(event)
            self.update_world()
            self.redraw_world()
        if self.game_over:
            self.display_message('GAME OVER')
        if self.exit:
            self.quit()
        else:
            self.reset_game()

    def quit(self):
        pg.quit()
        quit()
