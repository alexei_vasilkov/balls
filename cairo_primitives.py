from math import pi, cos, sin, atan2
from cairo import LINE_CAP_ROUND, LINE_CAP_BUTT, LINE_JOIN_ROUND
from test_bot import norm
PI2 = pi*2

def message(ctx, pos, msg, size=30, c=(0, 0, 0)):
    ctx.set_font_size(size)
    ctx.set_source_rgb(c[0], c[1], c[2])
    ctx.move_to(pos[0], pos[1])
    ctx.show_text(msg)


def circle(ctx, cell, *kwargs):
    ctx.set_line_join(LINE_JOIN_ROUND)
    s = 0
    e = PI2
    pos = cell.pos
    rad = cell.rad_wo_wall
    fill_c = cell.inside_color
    b_c = cell.wall_color
    w = cell.wall_w
    angle1 = 0
    angle2 = PI2


    ctx.set_line_width(w)
    ctx.set_source_rgb(b_c[0], b_c[1], b_c[2])

    #move origin
    #ctx.translate(w/2, h/2)
    ctx.arc(pos[0], pos[1], rad, angle1, angle2)
    ctx.stroke_preserve()
    ctx.set_source_rgb(fill_c[0], fill_c[1], fill_c[2])
    ctx.fill()
    ctx.set_source_rgb(0, 0, 0)
    ctx.set_line_width(2)
    ctx.move_to(pos[0], pos[1])
    rad += 10
    #ctx.line_to(pos[0] + rad*cos(cell.orientation), pos[1] + rad*sin(cell.orientation))
    di = cell.vel/norm(cell.vel)
    ctx.line_to(pos[0] + rad*di[0], pos[1] + rad*di[1])
    ctx.stroke()
    #draw_spikes(ctx, pos, rad + w/2)



def circle_(ctx, pos, rad, fill_c=(0, 1, 0), b_c=(0, 0, 0), w=1, angle1=0, angle2=PI2):

    ctx.set_line_width(w)
    ctx.set_source_rgb(b_c[0], b_c[1], b_c[2])

    #move origin
    #ctx.translate(w/2, h/2)
    ctx.arc(pos[0], pos[1], rad, angle1, angle2)
    ctx.stroke_preserve()
    ctx.set_source_rgb(fill_c[0], fill_c[1], fill_c[2])
    ctx.fill()
    draw_spikes(ctx, pos, rad + w/2)

def get_neighs_con_key(co1, co2, cell):
    if co1.c1 == cell:
        con_key = co1.c2
    else:
        con_key = co1.c1
    if co2.c1 == cell:
        con_key = (con_key, co2.c2)
    else:
        con_key = (con_key, co2.c1)

    if con_key[0].id < con_key[1].id:
        #print(con_key[0].id, con_key[1].id)
        return con_key
    else:
        #print(con_key[1].id, con_key[0].id)
        return con_key[1], con_key[0]


def test(ctx):
    pos = [500, 500]
    rad = 50
    w = 8
    ctx.set_line_width(w)
    ctx.set_source_rgb(0, 0, 1)

    #move origin
    #ctx.translate(w/2, h/2)
    a1 = 0
    a2 = PI2
    ctx.arc_negative(pos[0], pos[1], rad, 0, -pi/2)
    ctx.arc_negative(pos[0], pos[1], rad, pi, pi/2)
    ctx.close_path()
    ctx.stroke_preserve()
    ctx.set_source_rgb(1, 0, 0,)
    ctx.fill()
    draw_spikes(ctx, pos, rad + w/2)


def draw_cell(ctx, cell, all_connections):
    ctx.set_line_join(LINE_JOIN_ROUND)
    s = 0
    e = PI2
    pos = cell.pos
    rad = cell.rad_wo_wall
    fill_c = cell.inside_color
    b_c = cell.wall_color
    w = cell.wall_w
    angles = []
    if cell.connections:
        for c in cell.connections:
            if c.wall:
                q1 = c.wall[0] - cell.pos
                q2 = c.wall[1] - cell.pos
                s = atan2(q1[1], q1[0])
                e = atan2(q2[1], q2[0])
                if e < 0:
                    e += PI2
                if s < 0:
                    s += PI2
                if e > s:
                    s, e = e, s
                if e + PI2 - s - pi < 0:
                    s, e = e, s

                angles.append((e, s, c))


        angles.sort(key=lambda x: x[0])


    new_a = []
    ln = len(angles)
    strange_connections = []
    if ln == 0:
        angles = [(s, e, None)]
    else:
        angles.append(angles[0])
        for i in range(len(angles) - 1):
            a1 = angles[i]
            a2 = angles[i + 1]
            point =  None
            if ln > 1:
                y = a1[0]
                yy = a1[1]
                x = a2[0]
                xx = a2[1]
                if (yy > x) and not (x - y < 0 and yy - y > 0 and xx - x > 0) or\
                        (x > yy and x - y > 0 and yy - y < 0):
                    co1 = a1[2]
                    co2 = a2[2]
                    if co1.c1 == cell:
                        key = co1.c2
                    else:
                        key = co1.c1
                    if co2.c1 == cell:
                        key = (key, co2.c2)
                    else:
                        key = (key, co2.c1)

                    point = (cell.pos + key[0].pos + key[1].pos) / 3

            new_a.append((a1[1], a2[0], point))
        angles = new_a

    #ctx.set_source_rgb(0.5, 0.2, 0.7)

    ctx.set_line_width(w)
    ctx.set_line_cap(LINE_CAP_ROUND)
    ctx.set_source_rgb(b_c[0], b_c[1], b_c[2])




    #move origin
    #ctx.translate(w/2, h/2)
    #for angle in angles:
    #    ctx.arc(pos[0], pos[1], rad, angle[0], angle[1])
    #ctx.arc_negative(pos[0], pos[1], rad, 0, -pi/2)
    #ctx.arc_negative(pos[0], pos[1], rad, pi, pi/2)

    #ctx.close_path()
    #ctx.stroke_preserve()
    #ctx.stroke()
    #ctx.set_source_rgb(fill_c[0], fill_c[1], fill_c[2])
    #ctx.fill()
    ctx.set_line_width(6)
    ctx.set_source_rgb(0.3, 0.8, 0.3)
    for angle in angles:
        if angle[2] is not None:
            ctx.move_to(cell.pos[0], cell.pos[1])
            ctx.line_to(angle[2][0], angle[2][1])
    ctx.stroke()
    ctx.set_line_width(8)
    ctx.set_source_rgb(1, 0, 0)
    #ctx.set_line_cap(LINE_CAP_BUTT)
    #should be rund cap
    #ctx.arc(pos[0], pos[1], rad + w/2, angle1, angle2)
    for angle in angles:
        if angle[2] is None:
            ctx.arc(pos[0], pos[1], cell.rad - 8/2, angle[0], angle[1])
        else:
            ctx.line_to(angle[2][0], angle[2][1])
    #ctx.arc_negative(pos[0], pos[1], cell.rad -2/2, 0, -pi/2)
    #ctx.arc_negative(pos[0], pos[1], cell.rad -2/2, pi, pi/2)
    ctx.close_path()
    ctx.stroke()
    ww = 0.5*w
    ctx.set_line_width(ww)
    ctx.set_source_rgb(0, 0, 0)
    for angle in angles:
        if angle[2] is None:
            ctx.arc(pos[0], pos[1], rad, angle[0], angle[1])
        else:
            #ctx.line_to(angle[2][0], angle[2][1])
            v = angle[2] - cell.pos
            v *= (1 - ww/norm(v))
            p = cell.pos + v
            ctx.line_to(p[0], p[1])
    #ctx.arc(pos[0], pos[1], rad, angle1, angle2)

    #ctx.arc_negative(pos[0], pos[1], rad, 0, -pi/2)
    #ctx.arc_negative(pos[0], pos[1], rad, pi, pi/2)

    ctx.close_path()

    ctx.stroke()
    #ctx.set_source_rgb(0, 0, 0)
    #ctx.move_to(pos[0], pos[1])
    #ctx.line_to(pos[0] + rad*cos(cell.angle), pos[1] + rad*sin(cell.angle))
    #ctx.stroke()
    #draw_spikes(ctx, pos, rad + w/2)

def draw_spikes(ctx, pos, rad):
    ctx.set_line_width(2)
    ctx.set_source_rgb(0, 0, 1)
    n = 10
    const = PI2/n
    for i in range(n):
        ctx.move_to(pos[0], pos[1])
        ctx.line_to(pos[0] + rad*cos(const*i), pos[1] + rad*sin(const*i))
    ctx.stroke()


def line(ctx, st, en, c=(0, 0, 0,), w=1):
    ctx.set_line_width(w)
    ctx.set_source_rgb(c[0], c[1], c[2])
    ctx.move_to(st[0], st[1])
    ctx.line_to(en[0], en[1])
    ctx.stroke()

def draw_connection(ctx, c):
    ctx.set_line_width(c.width)
    co = c.color
    st = c.c1.pos
    en = c.c2.pos
    ctx.set_source_rgb(co[0], co[1], co[2])
    ctx.move_to(st[0], st[1])
    ctx.line_to(en[0], en[1])
    ctx.stroke()



