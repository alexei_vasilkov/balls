from math import ceil, floor
from collections import defaultdict

class OObject:

    def __init__(self, id, x, y, rad):
        self.id = id
        self.pos = [x, y]
        self.rad = rad

def get_collisions(objects, min_rad):
    #order of objects is important
    #min rad
    cell_size = 1.414*min_rad
    cells = defaultdict(list)
    for q in objects:
        q.key = (floor(q.pos[0]/cell_size), floor(q.pos[1]/cell_size))
        cells[q.key].append(q)
    collisions = []
    for i in range(len(objects)):
        Ri = ceil(2*objects[i].rad / cell_size)
        obj_i = objects[i]
        for x in range(-Ri, Ri + 1):
            for y in range(-Ri, Ri + 1):
                k = (obj_i.key[0] + x, obj_i.key[1] + y)
                if k not in cells:
                    continue
                for obj in cells[k]:
                    if obj_i.rad > obj.rad or (obj_i.rad == obj.rad and obj_i.id < obj.id):
                        if (obj_i.pos[0] - obj.pos[0])*(obj_i.pos[0] - obj.pos[0]) + (obj_i.pos[1] -
                                obj.pos[1])*(obj_i.pos[1] - obj.pos[1]) -\
                                (obj_i.rad + obj.rad)*(obj_i.rad + obj.rad) < 0.1:
                                    if obj_i.id < obj.id:
                                        collisions.append((obj_i, obj))
                                    else:
                                        collisions.append((obj, obj_i))
    return collisions


