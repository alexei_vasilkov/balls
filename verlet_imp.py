import pygame as pg
import numpy as np
from numpy import linalg as la
import itertools
from math import atan2, pi, sin, cos, ceil, sqrt
import time
from quad_tree import QuadTree
from test_bot import compute_circle_walls
from cairo_primitives import circle, line, draw_cell, draw_connection, test
import random
from c_quad_tree import QuadTree as cQuadTree
from profilehooks import profile
from lvl_boxing import get_collisions

c_q_time = 0
p_q_time = 0
bf_q_time = 0
counter = 0

PI2 = pi*2
count = itertools.count()
con_count = itertools.count()
def ord_pair(q1, q2):
    if q1.id > q2.id:
        return q2, q1
    else:
        return q1, q2

def ssd(arr):
    #sum of square differences
    return arr[0]*arr[0] + arr[1]*arr[1]

def norm(arr):
    return sqrt(arr[0]*arr[0] + arr[1]*arr[1])

def check_stillness(pos1, pos2):
    pass

def same_arr(a1, a2):
    return a1[0] == a2[0] and a2[1] == a2[1]

class WorldLawsEngine:

    def __init__(self, world_size):
        self.t = 0
        self.time_step = 0.02
        self.pixel_to_unit = 1.0
        self.border = (np.array([0.0, 0.0]), world_size)
        self.time_to_update = 0.0
        self.env_resistance = 0.015
        self.bounce_dumping = 0.05
        #TODO
        self.iteration_depth = 1
        self.quad_tree = QuadTree(0, np.array([0, 0]), np.array(world_size))
        self.c_quad_tree = cQuadTree(0, 0, world_size[0], world_size[1])
        offset = 200
        pos = (world_size[0]/2 - offset, world_size[1]/2)
        rad = 50
        self.circle_walls = []
        self.min_rad = 1000000000

        self.connections = {}
        #TODO make ordered dict
        self.cells = []
        offset = 100
        #self.cells.append(Cell([world_size[0]/2 - offset, world_size[1]/2], [3, 0], 50, 10))
        #self.cells.append(Cell([world_size[0]/2 + offset, world_size[1]/2], [-3, +3], 50, 8))
        #self.cells.append(Cell([world_size[0]/2, world_size[1]/2 + 150], [0, -1], 50, 8))


        #self.cells.append(Cell([world_size[0]/2 - offset, world_size[1]/2], [3, 0], 70, 10))
        #self.cells.append(Cell([world_size[0]/2 + offset, world_size[1]/2+30], [-3, 0], 60, 8))
        #self.cells.append(Cell([world_size[0]/2, world_size[1]/2 + 100], [0, -3], 40, 8))

        self.cells.append(Cell([world_size[0]/2 - offset, world_size[1]/2], [2, 0], 40, 10))
        self.cells.append(Cell([world_size[0]/2 + offset, world_size[1]/2], [-6, 0], 30, 80))
        self.cells.append(Cell([world_size[0]/2, world_size[1]/2 + 100], [0, -2], 40, 8))
        """
        n = 15
        m = 15
        r = 8
        speed = 6.5
        speed = 5
        self.min_rad = r
        for j in range(n):
            for i in range(m):
                self.cells.append(Cell([((world_size[0] - 2*r)/m*(i) + r + 10), (10 + r +
                    (world_size[1] - 2*r)/n*(j))], [random.random()*2 - 1, speed], r, 5))
        """
        #for i in range(6):
        #    self.cells.append(Cell([world_size[0]/2 - offset -50 + 2*offset*(i/4), 40], [0, 10], 30, 5))

        #r = 32
        #for j in range(0, m):
        #    for i in range(n):
        #        self.cells.append(Cell([r+1+2*i*r, r+1+2*j*r], [random.random()*10 - 5, 10], r - 20, 5))
        self.cell_dic = {}
        for cell in self.cells:
            self.cell_dic[cell.id] = cell
            cell.set_add_params(self.time_step, self.pixel_to_unit, self.env_resistance,
                    self.bounce_dumping)

        #self.cells.append(Cell([world_size[0]/2 - 100, world_size[1]/2], [0, 0], 3, 100))
        #self.cells.append(Cell([world_size[0]/2 + 100, world_size[1]/2], [0, 0], 3, 100))
        self.world_size = world_size


    def add_cell(self, pos, speed, rad, weight):
        if self.min_rad > rad:
            self.min_rad = rad
        self.cells.append(Cell(pos, speed, rad, weight))
        self.cell_dic[self.cells[-1].id] = self.cells[-1]
        self.cells[-1].set_add_params(self.time_step, self.pixel_to_unit, self.env_resistance,
                    self.bounce_dumping)


    def enforce_boundaries(self):
        for cell in self.cells:
            if cell.pos[0] + cell.rad > self.border[1][0]:
                cell.flip_dir(0, self.border[1][0] - cell.rad)
            elif cell.pos[0] - cell.rad < self.border[0][0]:
                cell.flip_dir(0, self.border[0][0] + cell.rad)

            if cell.pos[1] + cell.rad > self.border[1][1]:
                cell.flip_dir(1, self.border[1][1] - cell.rad)
            elif cell.pos[1] - cell.rad < self.border[0][1]:
                cell.flip_dir(1, self.border[0][1] + cell.rad)


    #@profile
    def update_state(self):
        self.t += self.time_step
        #print(len(self.cells), len(self.connections))
        self.update_cells_pos()
        self.satisfy_constraints()
        self.process_collisions()

    #@profile
    def satisfy_constraints(self):
        """
        The number of necessary iterations varies depending on the physical system simulated and the amount of motion.
        It can be made adaptive by measuring the change from last iteration. If we stop the iterations early,
        the result might not end up being quite valid but because of the Verlet scheme,
        in next frame it will probably be better, next frame even more so etc.
        This means that stopping early will not ruin everything although the resulting animation might appear somewhat sloppier.
        """
        #TODO maybe check if nothing changes - break the loop
        self.enforce_boundaries()
        to_remove = []
        for key, connection in self.connections.items():
            if not connection.satisfy_constraints():
                to_remove.append(key)
        if len(to_remove) > 0:
            for k in to_remove:
                del self.connections[k]

    def find_cell_by_point(self, pos, empty_space = 2):
        pos = np.array(pos)
        for cell in self.cells:
            if ssd(pos - cell.pos) < (cell.rad + empty_space)**2:
                #cell.pos -= np.random.rand(2)*10 - 5
                return cell
        return None

    def add_connection(self, key, strength=0.5, eq_len=None):
        if key in self.connections:
            return
            #print('check add spring')
        #TODO c = connection() so that only one call [key]
        self.connections[key] = Connection(key[0], key[1], self.time_step, strength, eq_len)
    #@profile
    def process_collisions(self):
        #print(self.cells[0].pos, self.cells[0].rad)
        #print(self.cells[1].pos, self.cells[1].rad)
        #print(self.cells[2].pos, self.cells[2].rad)
        #global c_q_time, p_q_time, counter, bf_q_time
        #counter += 1
        #quad = True
        #quad = False
        #c_quad = True
        #c_quad = False
        #b = time.time()
        """
        for cell in self.cells:
            self.c_quad_tree.insert_cell(cell)
        potentials = self.c_quad_tree.get_quads()
        for pots in potentials:
            for i in range(len(pots) - 1):
                for j in range(i + 1, len(pots)):
                    cell = self.cell_dic[pots[i]]
                    pot = self.cell_dic[pots[j]]
                    ordp = ord_pair(cell, pot)
                    if cell.id != pot.id and cell.cell_collision(pot):
                        #co += 1
                        self.add_connection(ordp, 1, (cell.rad + pot.rad)*0.8)
        self.c_quad_tree.clear()
        """


        #c_q_time += time.time() - b

        #b = time.time()
        collisions = get_collisions(self.cells, self.min_rad)
        for col in collisions:
            self.add_connection(col, 1, (col[0].rad + col[1].rad)*0.9)

        """
        co = 0
        for cell in self.cells:
            self.quad_tree.insert(cell)
        checked = set()
        for cell in self.cells:
            potentials = []
            self.quad_tree.retrieve(potentials, cell)
            for pot in potentials:
                ordp = ord_pair(cell, pot)
                co += 1
                if cell.id != pot.id and ordp not in checked and cell.cell_collision(pot):
                    self.add_connection(ordp, 1, (cell.rad + pot.rad)*0.8)
                checked.add(ordp)

        self.quad_tree.clear()
        print(len(self.cells), co)
        """
        """
        for cell in self.cells:
            self.quad_tree.insert(cell)
        potentials = self.quad_tree.get_quads()
        print(potentials)
        for pots in potentials:
            for i in range(len(pots) - 1):
                for j in range(i + 1, len(pots)):
                    cell = pots[i]
                    pot = pots[j]
                    ordp = ord_pair(cell, pot)
                    if cell.id != pot.id and cell.cell_collision(pot):
                        #co += 1
                        self.add_connection(ordp, 1, (cell.rad + pot.rad)*0.8)
        self.quad_tree.clear()

        p_q_time += time.time() - b

        b = time.time()
        for i in range(len(self.cells) - 1):
            for j in range(i + 1, len(self.cells)):
                cell = self.cells[i]
                pot = self.cells[j]
                ordp = ord_pair(cell, pot)
                if cell.id != pot.id and cell.cell_collision(pot):
                    #co += 1
                    pass
                    #self.add_connection(ordp, 1, (cell.rad + pot.rad)*0.8)
        bf_q_time += time.time() - b

        if len(self.cells) % 20 == 0:
            print(len(self.cells))
            print('Python cumulative time %0.2f' % p_q_time)
            print('C++ cumulative time %0.2f' % c_q_time)
            print('Bruteforce cumulative time %0.2f' % bf_q_time)
        """


        #print(len(self.cells), co)
    #@profile
    def draw(self, ctx):
        #test(ctx)
        for cell in self.cells:
            #circle(ctx, cell, self.connections)
            draw_cell(ctx, cell, self.connections)
        for connection in self.connections.values():
            draw_connection(ctx, connection)

    def update_cells_pos(self):
        for cell in self.cells:
            cell.update_pos()



class Connection:

    def __init__(self, cell1, cell2, time_step, strength=0.01, eq_len=None):
        #TODO CONTROL FORCE CHOOSE WISELY
        self.id = next(con_count)
        self.c1 = cell1
        self.c2 = cell2
        self.c1.connections.add(self)
        self.c2.connections.add(self)
        #self.wall = None
        self.wall = compute_circle_walls(self.c1, self.c2)
        self.color = (1, 0.0, 0.8)
        self.width = 6
        self.prev_force = 100**2
        if eq_len is None:
            self.eq_len = norm(cell1.pos - cell2.pos)
        else:
            self.eq_len = eq_len
        self.strength = strength
        #self.strength = 0.1
        self.strength = 1
        self.eq_len_sq = self.eq_len**2
        self.time_step = time_step/time_step
        self.broke_force = 1
        self.broke_force **= 2
        self.dist = 100
        #TODO
        self.tear = 1.05*(cell1.rad + cell2.rad)

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    def satisfy_constraints(self):
        """
        axis = self.c2.pos - self.c1.pos
        cur_dist = norm(axis)
        unit_axis = axis/cur_dist

        vel1 = self.c1.pos - self.c1.prev_pos
        vel2 = self.c2.pos - self.c2.prev_pos
        rel_vel = (vel2 - vel1).dot(unit_axis)

        rel_dist = cur_dist - self.eq_len

        remove = rel_vel + rel_dist/self.time_step

        impulse = remove/(self.c1.inv_mass + self.c2.inv_mass)

        I = unit_axis*impulse

        self.c1.prev_pos = self.c1.pos - (vel1 + I*self.c1.inv_mass)
        print(I*self.c1.inv_mass)
        print(I*self.c2.inv_mass)
        self.c2.prev_pos = self.c2.pos - (vel1 - I*self.c2.inv_mass)
        return True
        """
        diff = self.c1.pos - self.c2.pos
        self.wall = compute_circle_walls(self.c1, self.c2)
        if self.wall is not None:
            dist = self.wall[-1]
        else:
            dist = norm(diff)
        dist = norm(diff)
        self.dist = dist
        if dist == 0:
            return True
        #if dist > self.tear:
        a = (self.c1.rad_sq - self.c2.rad_sq + dist**2)/(2*dist)
        if a < self.c1.rad*0.2 or dist - a < self.c2.rad*0.2:
            self.c1.collide(self.c2, dist)
        if dist > self.c1.rad + self.c2.rad:
            self.c1.connections.remove(self)
            self.c2.connections.remove(self)
            return False
        difference = (self.eq_len - dist) / dist
        #translate = diff * 0.5 * difference
        #maybe add iterations
        #if abs(difference) > 0.1:
        #    translate = diff * 0.5 * difference
        #else:
        #    translate = diff * difference
        im1 = 1 / self.c1.mass
        im2 = 1 / self.c2.mass
        sc1 = (im1 / (im1 + im2)) * self.strength
        sc2 = self.strength - sc1

        translate = diff * difference
        if difference < -2:
            return False
        self.c1.pos += translate*sc1
        self.c2.pos -= translate*sc2
        return True


class Cell:

    def __init__(self, pos, vel, rad, mass):
        self.pos = np.array(pos).astype(float)
        self.connections = set()
        self.inside_color = (1, 0, 0)
        self.wall_color = (34/255, 139/255, 34/255)
        self.wall_w = 4
        self.rad_wo_wall = rad
        self.orientation = -pi/2
        #TODO
        self.rad = rad + self.wall_w/2
        self.rad_sq = self.rad**2
        self.vel = np.array(vel)
        self.prev_pos = self.pos - vel
        #self.vel = np.array(vel).astype(float)
        self.mass = mass
        self.inv_mass = 1.0/mass
        self.acc = np.array([0, 0]).astype(float)
        self.id = next(count)
        self.time_step = None
        self.pixel_to_unit = None
        self.rect_size = np.array([2*rad, 2*rad]).astype(int)
        self.env_resistance = None
        self.bounce_dumping = None
        #self.gravity = np.array([0, 9.8])
        #self.gravity = np.array([0, 0.01])
        self.gravity = np.array([0.0, 0.0])
        #self.acc = self.gravity
        self.int_pos = None


    def flip_dir(self, d, correction):
        vel = (self.pos[d] - self.prev_pos[d])*self.bounce_dumping
        self.pos[d], self.prev_pos[d] = correction, correction + vel

    def update_connections(self):
        pass

    def set_add_params(self, time_step, pixel_to_unit, env_resistance, bounce_dumping):
        #TODO LOL
        self.time_step = time_step
        self.time_step_sq = time_step**2
        #self.time_step = time_step
        #self.time_step_sq = time_step**2
        self.pixel_to_unit = pixel_to_unit
        self.env_resistance = (1 - env_resistance)
        self.bounce_dumping = (1 - bounce_dumping)

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    def top_left(self):
        return (self.pos[0] - self.rad, self.pos[1] - self.rad)

    def cell_collision(self, cell):
        return ssd(self.pos - cell.pos) < (self.rad + cell.rad)**2

    def update_pos(self):
        p = self.pos
        delta = (self.pos - self.prev_pos)*self.env_resistance + self.acc*self.time_step_sq
        if abs(delta[0]) < 0.01:
            delta[0] = 0
        if abs(delta[1]) < 0.01:
            delta[1] = 0
        self.pos = self.pos + delta
        self.vel = self.pos - p
        self.prev_pos = p

        #print(self.pos, self.prev_pos)

    def accumulate_forces(self):
        #self.acc = self.gravity
        pass

    def collide(self, cell, dist):
        dr = self.pos - cell.pos
        s_vel = self.vel
        c_vel = cell.vel
        dv = s_vel - c_vel
        dot = dr.dot(dv.T)
        #objects don't move toward each other
        if dot > 0:
            return
        #dist = la.norm(dr)
        j = 2*self.mass*cell.mass*dot/dist/(self.mass + cell.mass)
        jj = j*dr/dist

        #self.pos -= s_vel
        #cell.pos -= c_vel
        s_vel -= jj/self.mass
        c_vel += jj/cell.mass
        s_vel *= self.bounce_dumping
        c_vel *= self.bounce_dumping
        #self.pos -= s_vel
        #cell.pos -= c_vel
        self.prev_pos = self.pos - s_vel
        cell.prev_pos = cell.pos - c_vel



    def __str__(self):
        return str(self.id)


