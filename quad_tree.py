import gc
import numpy as np

class QuadTree:

    def __init__(self, level, top_left, size, max_objects=5, max_levels=5):
        self.level = level
        #(x,y), (w,h)
        self.top_left = top_left
        self.size = size
        #TODO maybe dict?
        self.objects = []
        self.nodes = []
        self.max_objects = max_objects
        self.max_levels = max_levels

    def clear(self):
        for node in self.nodes:
            node.clear()
        self.objects = []
        self.nodes = []

    def split(self):
        #sub_size
        p = self.top_left
        ss = self.size//2
        self.nodes.append(QuadTree(self.level + 1, (p[0] + ss[0], p[1]), ss))
        self.nodes.append(QuadTree(self.level + 1, p, ss))
        self.nodes.append(QuadTree(self.level + 1, (p[0], p[1] + ss[1]), ss))
        self.nodes.append(QuadTree(self.level + 1, p + ss, ss))

    def get_index(self, obj):
        #top left corner coordinates
        index = -1
        midpoint = self.top_left + self.size/2

        tl = obj.top_left()
        size = obj.rect_size

        topQ = tl[1] + size[1] < midpoint[1]
        bottomQ = tl[1] > midpoint[1]

        if tl[0] + size[0] < midpoint[0]:
            if topQ:
                index = 1
            elif bottomQ:
                index = 2
        elif tl[0] > midpoint[0]:
            if topQ:
                index = 0
            elif bottomQ:
                index = 3

        return index

    def get_quads(self):#, lst):
        #if len(self.objects) > 0:
        #    lst.append(self.objects[-1])

        #if len(self.nodes) == 0:
        #    return
        #self.nodes[0].get_quads(lst)
        #self.nodes[1].get_quads(lst)
        #self.nodes[2].get_quads(lst)
        #self.nodes[3].get_quads(lst)

        if len(self.nodes) == 0:
            return self.objects
        self.objects.append(self.nodes[0].get_quads())
        self.objects.append(self.nodes[1].get_quads())
        self.objects.append(self.nodes[2].get_quads())
        self.objects.append(self.nodes[3].get_quads())
        return self.objects



    def insert(self, obj):

        if len(self.nodes) > 0:
            index = self.get_index(obj)
            if index != -1:
                self.nodes[index].insert(obj)
                return

        self.objects.append(obj)

        if len(self.objects) > self.max_objects and self.level < self.max_levels:

            if len(self.nodes) == 0:
                self.split()

            i = 0
            while i < len(self.objects):
                index = self.get_index(self.objects[i])
                if index != -1:
                    self.nodes[index].insert(self.objects.pop(i))
                else:
                    i += 1

    def retrieve(self, return_objects, obj):
        index = self.get_index(obj)
        if index != -1 and len(self.nodes) > 0:
            self.nodes[index].retrieve(return_objects, obj)

        return_objects += self.objects

        return return_objects
