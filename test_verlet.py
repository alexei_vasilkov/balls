import pygame as pg
import numpy as np
from numpy import linalg as la
import itertools
from math import atan2, pi, sin, cos, ceil, sqrt
import time
from quad_tree import QuadTree
from test_bot import compute_circle_walls
from cairo_primitives import circle, line, draw_cell, draw_connection, test
import random

PI2 = pi*2
#from profilehooks import profile
count = itertools.count()
con_count = itertools.count()
def ord_pair(q1, q2):
    if q1.id > q2.id:
        return q2, q1
    else:
        return q1, q2

def ssd(arr):
    #sum of square differences
    return arr[0]*arr[0] + arr[1]*arr[1]

def norm(arr):
    return sqrt(arr[0]*arr[0] + arr[1]*arr[1])

def check_stillness(pos1, pos2):
    pass

def same_arr(a1, a2):
    return a1[0] == a2[0] and a2[1] == a2[1]

class WorldLawsEngine:

    def __init__(self, world_size):
        self.t = 0
        self.time_step = 0.02
        self.pixel_to_unit = 1.0
        self.border = (np.array([0.0, 0.0]), world_size)
        self.time_to_update = 0.0
        self.env_resistance = 0.015
        self.bounce_dumping = 0.05
        #TODO
        self.iteration_depth = 1
        self.quad_tree = QuadTree(0, np.array([0, 0]), np.array(world_size))
        offset = 200
        pos = (world_size[0]/2 - offset, world_size[1]/2)
        rad = 50
        self.circle_walls = []

        self.connections = {}
        #TODO make ordered dict
        self.cells = []
        offset = 100
        #self.cells.append(Cell([world_size[0]/2 - offset, world_size[1]/2], [3, 0], 50, 10))
        #self.cells.append(Cell([world_size[0]/2 + offset, world_size[1]/2], [-3, +3], 50, 8))
        #self.cells.append(Cell([world_size[0]/2, world_size[1]/2 + 150], [0, -1], 50, 8))


        #self.cells.append(Cell([world_size[0]/2 - offset, world_size[1]/2], [3, 0], 70, 10))
        #self.cells.append(Cell([world_size[0]/2 + offset, world_size[1]/2+30], [-3, 0], 60, 8))
        #self.cells.append(Cell([world_size[0]/2, world_size[1]/2 + 100], [0, -3], 40, 8))

        self.cells.append(Cell([world_size[0]/2 - offset, world_size[1]/2], [2, 0], 40, 10))
        self.cells.append(Cell([world_size[0]/2 + offset, world_size[1]/2], [-2, 0], 30, 8))
        self.cells.append(Cell([world_size[0]/2, world_size[1]/2 + 100], [0, -2], 40, 8))
        #for i in range(6):
        #    self.cells.append(Cell([world_size[0]/2 - offset -50 + 2*offset*(i/4), 40], [0, 10], 30, 5))

        #n =12
        #m = 8
        #r = 32
        #for j in range(0, m):
        #    for i in range(n):
        #        self.cells.append(Cell([r+1+2*i*r, r+1+2*j*r], [random.random()*10 - 5, 10], r - 20, 5))

        for cell in self.cells:
            cell.set_add_params(self.time_step, self.pixel_to_unit, self.env_resistance,
                    self.bounce_dumping)

        #self.cells.append(Cell([world_size[0]/2 - 100, world_size[1]/2], [0, 0], 3, 100))
        #self.cells.append(Cell([world_size[0]/2 + 100, world_size[1]/2], [0, 0], 3, 100))
        self.world_size = world_size


    def add_cell(self, pos, speed, rad, weight):
        self.cells.append(Cell(pos, speed, rad, weight))
        self.cells[-1].set_add_params(self.time_step, self.pixel_to_unit, self.env_resistance,
                    self.bounce_dumping)


    def enforce_boundaries(self):
        for cell in self.cells:
            if cell.pos[0] + cell.rad > self.border[1][0]:
                cell.flip_dir(0, self.border[1][0] - cell.rad)
            elif cell.pos[0] - cell.rad < self.border[0][0]:
                cell.flip_dir(0, self.border[0][0] + cell.rad)

            if cell.pos[1] + cell.rad > self.border[1][1]:
                cell.flip_dir(1, self.border[1][1] - cell.rad)
            elif cell.pos[1] - cell.rad < self.border[0][1]:
                cell.flip_dir(1, self.border[0][1] + cell.rad)


    def update_state(self):
        self.t += self.time_step
        #print(len(self.cells), len(self.connections))
        self.update_cells_pos()
        self.satisfy_constraints()
        self.process_collisions()

    #@profile
    def satisfy_constraints(self):
        """
        The number of necessary iterations varies depending on the physical system simulated and the amount of motion.
        It can be made adaptive by measuring the change from last iteration. If we stop the iterations early,
        the result might not end up being quite valid but because of the Verlet scheme,
        in next frame it will probably be better, next frame even more so etc.
        This means that stopping early will not ruin everything although the resulting animation might appear somewhat sloppier.
        """
        #TODO maybe check if nothing changes - break the loop
        self.enforce_boundaries()
        #cell.pos = np.minimum(np.maximum(cell.pos, self.border[0] + cell.rad),
        #        self.border[1] - cell.rad)
        #print(cell.pos, cell.prev_pos)
        to_remove = []
        for key, connection in self.connections.items():
            if not connection.satisfy_constraints():
                to_remove.append(key)
        if len(to_remove) > 0:
            for k in to_remove:
                del self.connections[k]
        #for i in range(self.n):
        #    self.cells[i].pos = self.fixed_pos[i]

    def find_cell_by_point(self, pos, empty_space = 2):
        pos = np.array(pos)
        for cell in self.cells:
            if ssd(pos - cell.pos) < (cell.rad + empty_space)**2:
                #cell.pos -= np.random.rand(2)*10 - 5
                return cell
        return None

    def add_connection(self, key, strength=0.5, eq_len=None):
        if key in self.connections:
            return
            #print('check add spring')
        #TODO c = connection() so that only one call [key]
        self.connections[key] = Connection(key[0], key[1], self.time_step, strength, eq_len)
    #@profile
    def process_collisions(self):
        
        quad = True
        co = 0
        for cell in self.cells:
            self.quad_tree.insert(cell)
        checked = set()
        for cell in self.cells:
            potentials = []
            self.quad_tree.retrieve(potentials, cell)
            for pot in potentials:
                ordp = ord_pair(cell, pot)
                if cell.id != pot.id and ordp not in checked and cell.cell_collision(pot):
                    #co += 1
                    self.add_connection(ordp, 1, (cell.rad + pot.rad)*0.8)
                checked.add(ordp)

        self.quad_tree.clear()
        """
        checked = set()
        for i in range(len(self.cells) - 1):
            for j in range(i + 1, len(self.cells)):
                cell = self.cells[i]
                pot = self.cells[j]
                ordp = ord_pair(cell, pot)
                if cell.id != pot.id and ordp not in checked and cell.cell_collision(pot):
                    #co += 1
                    self.add_connection(ordp, 1, (cell.rad + pot.rad)*0.8)
                checked.add(ordp)
        """


        #print(len(self.cells), co)
    #@profile
    def draw(self, ctx):
        #test(ctx)
        for cell in self.cells:
            draw_cell(ctx, cell, self.connections)
        for connection in self.connections.values():
            draw_connection(ctx, connection)

    def update_cells_pos(self):
        for cell in self.cells:
            cell.update_pos()



class Connection:

    def __init__(self, cell1, cell2, time_step, strength=0.01, eq_len=None):
        #TODO CONTROL FORCE CHOOSE WISELY
        self.id = next(con_count)
        self.c1 = cell1
        self.c2 = cell2
        self.c1.connections.add(self)
        self.c2.connections.add(self)
        #self.wall = None
        self.wall = compute_circle_walls(self.c1, self.c2)
        self.color = (1, 0.0, 0.8)
        self.width = 6
        self.prev_force = 100**2
        if eq_len is None:
            self.eq_len = norm(cell1.pos - cell2.pos)
        else:
            self.eq_len = eq_len
        self.strength = strength
        self.strength = 0.3
        self.eq_len_sq = self.eq_len**2
        self.time_step = time_step/time_step
        self.broke_force = 1
        self.broke_force **= 2

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    def satisfy_constraints(self):
        #if self.c1.pos[0] == self.c1.prev_pos[0] and self.c1.pos[1] == self.c1.prev_pos[1] and\
        #        self.c2.pos[0] == self.c2.prev_pos[0] and self.c2.pos[1] == self.c2.prev_pos[1]:
        #    return
        """
        dr = self.c1.pos - self.c2.pos
        self.wall = compute_circle_walls(self.c1, self.c2)
        if self.wall:
            dist = self.wall[3]
            if self.wall[2] < self.c1.rad*0.2 or dist - self.wall[2] < self.c2.rad*0.2:
                self.c1.collide(self.c2, dist)
        else:
            dist = norm(dr)
        force = self.eq_len - dist
        if dist > self.c1.rad + self.c2.rad:
            self.c1.connections.remove(self)
            self.c2.connections.remove(self)
            return False
        force *= self.strength
        direc = dr/dist
        self.c1.pos += direc*force/self.c1.mass
        self.c2.pos -= direc*force/self.c2.mass
        return True
        """
        delta = self.c2.pos - self.c1.pos
        self.wall = compute_circle_walls(self.c1, self.c2)
        if self.wall:
            dist = self.wall[3]
            if self.wall[2] < self.c1.rad*0.2 or dist - self.wall[2] < self.c2.rad*0.2:
                self.c1.collide(self.c2, dist)
        else:
            dist = norm(delta)
        if dist > self.c1.rad + self.c2.rad:
            self.c1.connections.remove(self)
            self.c2.connections.remove(self)
            return False
        diff = (dist - self.eq_len)/dist
        self.c1.pos += delta*0.5*diff
        self.c2.pos -= delta*0.5*diff
        #delta*=self.eq_len_sq/(delta*delta+self.eq_len_sq)-0.5;
        #self.c1.pos += delta
        #self.c2.pos -= delta
        return True


class Cell:

    def __init__(self, pos, vel, rad, mass):
        self.pos = np.array(pos).astype(float)
        self.connections = set()
        self.inside_color = (1, 0, 0)
        self.wall_color = (34/255, 139/255, 34/255)
        self.wall_w = 15
        self.rad_wo_wall = rad
        #TODO
        self.rad = rad + self.wall_w/2
        self.rad_sq = self.rad**2
        self.prev_pos = self.pos - vel
        #self.vel = np.array(vel).astype(float)
        self.mass = mass
        self.acc = np.array([0, 0]).astype(float)
        self.id = next(count)
        self.time_step = None
        self.pixel_to_unit = None
        self.rect_size = np.array([2*rad, 2*rad]).astype(int)
        self.env_resistance = None
        self.bounce_dumping = None
        #self.gravity = np.array([0, 9.8])
        #self.gravity = np.array([0, 0.01])
        self.gravity = np.array([0.0, 0.0])
        #self.acc = self.gravity
        self.int_pos = None


    def flip_dir(self, d, correction):
        vel = (self.pos[d] - self.prev_pos[d])*(1 - self.bounce_dumping)
        self.pos[d], self.prev_pos[d] = correction, correction + vel

    def update_connections(self):
        pass

    def set_add_params(self, time_step, pixel_to_unit, env_resistance, bounce_dumping):
        #TODO LOL
        self.time_step = time_step
        self.time_step_sq = time_step**2
        #self.time_step = time_step
        #self.time_step_sq = time_step**2
        self.pixel_to_unit = pixel_to_unit
        self.env_resistance = env_resistance
        self.bounce_dumping = bounce_dumping

    def __hash__(self):
        return self.id

    def __eq__(self, other):
        return self.id == other.id

    def top_left(self):
        return (self.pos - self.rad)

    def cell_collision(self, cell):
        return ssd(self.pos - cell.pos) < (self.rad + cell.rad)**2

    def update_pos(self):
        #self.accumulate_forces()
        #TODO replace all 1 - coeff with new_coeff*
        vel = self.pos - self.prev_pos
        self.pos, self.prev_pos = self.pos + (1 - self.env_resistance)*vel + self.acc*self.time_step_sq, self.pos
        self.acc[0] = 0
        self.acc[1] = 0

        if abs(vel[0]) < 0.002:
            self.prev_pos[0] = self.pos[0]
        if abs(vel[1]) < 0.002:
            self.prev_pos[1] = self.pos[1]

    def accumulate_forces(self):
        #self.acc = self.gravity
        pass

    def collide(self, cell, dist):
        dr = self.pos - cell.pos
        s_vel = self.pos - self.prev_pos
        c_vel = cell.pos - cell.prev_pos
        dv = s_vel - c_vel
        dot = dr.dot(dv.T)
        if not dot < 0:
            return
        #dist = la.norm(dr)
        j = 2*self.mass*cell.mass*dot/dist/(self.mass + cell.mass)
        jj = j*dr/dist

        #self.pos -= s_vel
        #cell.pos -= c_vel
        s_vel -= jj/self.mass
        c_vel += jj/cell.mass
        s_vel *= (1.0 - self.bounce_dumping)
        c_vel *= (1.0 - self.bounce_dumping)
        #self.pos -= s_vel
        #cell.pos -= c_vel
        self.prev_pos = self.pos - s_vel
        cell.prev_pos = cell.pos - c_vel


