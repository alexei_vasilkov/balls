import sys
import pantograph
import random
import math
import itertools
from html5_verlet import WorldLawsEngine

class BouncingBallDemo(pantograph.PantographHandler):
    def setup(self):
        self.engine = WorldLawsEngine(self, [self.width, self.height])
        self.xvel = random.randint(1, 5)
        self.yvel = random.randint(1, 5)

    def update(self):
        self.clear_rect(0, 0, self.width, self.height)
        self.engine.update_state()

if __name__ == '__main__':
    app = pantograph.SimplePantographApplication(BouncingBallDemo)
    app.run()
